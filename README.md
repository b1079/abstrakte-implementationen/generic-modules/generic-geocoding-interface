# Generic Geocoding Interface:

## Inhalt:
Enthält die Implementation der generischen Interfaces für die Apis:

* HERE Geocoding API
* Google Geocoding API
* Nominatim

## Setup
### HERE Geocoding API
Um HERE Geocoding APIfunktionsfähig zu bekommen müssen folgende Settings gesetzt werden:
* ``here.apikey`` mit einem validen APIKey für HereAPI
### Google Geocoding API
Um Google Geocoding API funktionsfähig zu bekommen müssen folgende Settings gesetzt werden:
* ``google.apikey`` mit einem validen API Key für Google Geocoding
### Nominatim Geocoding
* ``nominatim.url`` mit einem Endpoint für Nominatim. Default: ``http://localhost:5450``
## API Limiting:

### Google Geocoding
Google Maps hat ein Limit von 100 Anfragen/s. Die Google Maven Implementation
versucht dieses selbstständig kontrollieren.

### HERE Geocoding
Die Geocoding API hat jedoch ein Limit von 5 Anfragen/s.
