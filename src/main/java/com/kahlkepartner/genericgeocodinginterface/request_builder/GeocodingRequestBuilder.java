package com.kahlkepartner.genericgeocodinginterface.request_builder;

import implementations.GeoCodingInterface;
import interfaces.LocationInformation;
import interfaces.data.Address;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import model.exeptions.GenericRoutingException;
import model.exeptions.ParsingException;
import model.geometry.GeoJSONPoint;

/**
 * Statische Hilfsklasse zum Bau eines Requests für die Geocoding Apis
 */
public class GeocodingRequestBuilder {
    private GeoJSONPoint location;
    private Address address;

    /**
     * Overwrites the location information of the object
     *
     * @param location location which should be geocoded in LatLn format
     * @return Requestbuilder
     */
    public GeocodingRequestBuilder setLocation(GeoJSONPoint location) {
        this.location = location;
        address = null;
        return this;
    }

    /**
     * Overwrites the location information with an address object
     *
     * @param address
     * @return
     */
    public GeocodingRequestBuilder setAddress(Address address) {
        this.address = address;
        this.location = null;
        return this;
    }


    public GeocodingRequestBuilder() {
    }

    /**
     * Executes the defined request through an implementation of the geocodingInterface
     *
     * @param geoCodingInterface
     * @return
     * @throws Exception whenever the API throws an Exception or if the locationInformation are empty within
     *                   the request
     */
    public LocationInformation execute(GeoCodingInterface geoCodingInterface) throws Exception {
        if (location != null) {
            return geoCodingInterface.getLocationInformation(location);
        }
        if (address != null)
            return geoCodingInterface.getLocationInformation(address);
        throw new GenericRoutingException("Could not find a given resource to search");
    }

}
