package com.kahlkepartner.genericgeocodinginterface.nominatim;

import com.kahlkepartner.nominatimapi.responses.SearchResponse;
import interfaces.AbstractLocationInformation;
import interfaces.data.Address;
import model.geometry.GeoJSONPoint;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Implementation of the LocationInformation for Nominatim
 */
public class NominatimLocationInformation extends AbstractLocationInformation implements Serializable {
    protected HashMap<String, Object> extraInformation = new HashMap<>();

    NominatimLocationInformation(SearchResponse response) {
        this.address = new Address();
        if (response.getAddress() != null) {
            this.address.setCountry(response.getAddress().getCountry());

            this.address.setCity(response.getAddress().getCity());
            this.address.setHouseNumber(response.getAddress().getHouseNumber());
            this.address.setStreet(response.getAddress().getStreet());
            this.address.setPostalCode(response.getAddress().getPostalCode());
            this.address.setState(response.getAddress().getState());
            extraInformation.put("municipality", response.getAddress().getMunicipality());
        }
        this.location = new GeoJSONPoint();
        location.setLatitude(Double.parseDouble(response.getLat()));
        location.setLongitude(Double.parseDouble(response.getLon()));
    }

    /**
     * Sets address of the local object. Only overwrites if no values are given from the response or default value is set.
     *
     * @param response
     */
    public void setAddress(Address response) {
        if (this.address.getCountry() == null || this.address.getCountry().equals(""))
            this.address.setCountry(response.getCountry());
        if (this.address.getCity() == null || this.address.getCity().equals(""))
            this.address.setCity(response.getCity());
        if (this.address.getHouseNumber() == null || this.address.getHouseNumber().equals(""))
            this.address.setHouseNumber(response.getHouseNumber());
        if (this.address.getStreet() == null || this.address.getStreet().equals(""))
            this.address.setStreet(response.getStreet());
        if (this.address.getPostalCode() == null || this.address.getPostalCode().equals(""))
            this.address.setPostalCode(response.getPostalCode());
        if (this.address.getState() == null || this.address.getState().equals(""))
            this.address.setState(response.getState());
    }

    @Override
    public HashMap<String, Object> getExtraInformation() {
        return extraInformation;
    }
}
