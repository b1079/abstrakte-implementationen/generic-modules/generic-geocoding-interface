package com.kahlkepartner.genericgeocodinginterface.nominatim;

import com.kahlkepartner.nominatimapi.services.ReverseSearchService;
import com.kahlkepartner.nominatimapi.services.SearchService;
import implementations.GeoCodingInterface;
import interfaces.LocationInformation;
import interfaces.data.Address;
import model.geometry.GeoJSONPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Service;

@Service
@ConditionalOnExpression("${nominatim.enabled:true}")
/**
 * Implementation of the Geocoding Interface for Nominatim
 */
public class NominatimGeocodeService implements GeoCodingInterface {
    @Autowired
    SearchService searchService;
    @Autowired
    ReverseSearchService reverseSearchService;

    @Override
    public LocationInformation getLocationInformation(Address address) throws Exception {
        var geocoded = searchService.geocodeAddress(com.kahlkepartner.nominatimapi.model.Address.fromAddressModel(address));
        if (geocoded.length == 0)
            return null;
        var data = new NominatimLocationInformation(geocoded[0]);
        data.setAddress(address);
        return data;
    }

    @Override
    public LocationInformation getLocationInformation(GeoJSONPoint geoJSONPoint) throws Exception {
        var geocoded = reverseSearchService.reverseGeocodeAddress(geoJSONPoint);

        return new NominatimLocationInformation(geocoded);
    }
}
