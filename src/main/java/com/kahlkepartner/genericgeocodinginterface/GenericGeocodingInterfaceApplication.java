package com.kahlkepartner.genericgeocodinginterface;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {
        "com.kahlkepartner.genericgeocodinginterface",
        "com.kahlkepartner.osrmrouting",
        "com.kahlkepartner.googleapi",
        "com.kahlkepartner.nominatimapi",
        "com.kahlkepartner.here_maps"
})
public class GenericGeocodingInterfaceApplication {

    public static void main(String[] args) {
        SpringApplication.run(GenericGeocodingInterfaceApplication.class, args);
    }

}
