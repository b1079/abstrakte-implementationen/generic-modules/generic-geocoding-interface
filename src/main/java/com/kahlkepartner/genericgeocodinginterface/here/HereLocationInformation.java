package com.kahlkepartner.genericgeocodinginterface.here;

import com.kahlkepartner.here_maps.geocoding.HereGeocoderResponse;
import interfaces.AbstractLocationInformation;
import interfaces.data.Address;
import model.geometry.GeoJSONPoint;

import java.io.Serializable;
import java.util.HashMap;

public class HereLocationInformation extends AbstractLocationInformation implements Serializable {
    /**
     * Creates the LocationInformation object from the API Response
     * @param response response of the here api
     */
    public HereLocationInformation(HereGeocoderResponse response) {
        var responses = response.getItems();
        if (!responses.isEmpty()) {
            var firstResponse = responses.get(0);
            address = new Address();
            address.setCountry(firstResponse.getAddress().getCountryName());
            address.setCity(firstResponse.getAddress().getCity());
            address.setHouseNumber(firstResponse.getAddress().getHouseNumber());
            address.setStreet(firstResponse.getAddress().getStreet());
            address.setPostalCode(firstResponse.getAddress().getPostalCode());
            address.setState(firstResponse.getAddress().getState());

            this.location = GeoJSONPoint.fromLngLat(firstResponse.getPosition().getLng(), firstResponse.getPosition().getLat());
        }
    }

    /**
     * possible to add parameter or other information into this hashmap
     * @return
     */
    @Override
    public HashMap<String, Object> getExtraInformation() {
        return null;
    }
}
