package com.kahlkepartner.genericgeocodinginterface.here;

import com.kahlkepartner.here_maps.geocoding.HereGeocoder;
import com.kahlkepartner.here_maps.geocoding.HereReverseGeocoder;
import implementations.GeoCodingInterface;
import interfaces.LocationInformation;
import interfaces.data.Address;
import model.geometry.GeoJSONPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Service;

@Service
@ConditionalOnExpression("${here.geocoder:true}")
public class HereGeocodingService implements GeoCodingInterface {
    HereGeocoder hereGeocoder;
    HereReverseGeocoder hereReverseGeocoder;

    public HereGeocodingService(HereGeocoder hereGeocoder, HereReverseGeocoder hereReverseGeocoder) {
        this.hereGeocoder = hereGeocoder;
        this.hereReverseGeocoder = hereReverseGeocoder;
    }

    @Override
    public LocationInformation getLocationInformation(Address address) throws Exception {
        var res = hereGeocoder.calculateLocation(address);
        return new HereLocationInformation(res);
    }

    @Override
    public LocationInformation getLocationInformation(GeoJSONPoint geoJSONPoint) throws Exception {
        var res = hereReverseGeocoder.calculateLocation(geoJSONPoint);
        return new HereLocationInformation(res);
    }
}
