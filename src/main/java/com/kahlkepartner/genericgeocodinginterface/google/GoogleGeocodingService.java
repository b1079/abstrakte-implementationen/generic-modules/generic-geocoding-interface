package com.kahlkepartner.genericgeocodinginterface.google;

import com.google.maps.errors.ApiException;
import com.kahlkepartner.googleapi.service.GoogleRouteService;
import implementations.GeoCodingInterface;
import interfaces.LocationInformation;
import interfaces.data.Address;
import model.geometry.GeoJSONPoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnExpression;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
/**
 * Implementation des Geocoding Interfaces für die Google API
 */
@ConditionalOnExpression("${google.enabled:true}")
public class GoogleGeocodingService implements GeoCodingInterface {
    @Autowired
    GoogleRouteService routeService;

    @Override
    public LocationInformation getLocationInformation(Address address) throws InterruptedException, ApiException, IOException, model.exeptions.ApiException {
        var query = buildQuery(address);
        var req = routeService.getLocations(query);
        if (req.length == 0)
            return null;

        return new GoogleLocationInformation(req[0]);
    }

    @Override
    public LocationInformation getLocationInformation(GeoJSONPoint geoJSONPoint) throws InterruptedException, ApiException, IOException {
        var req = routeService.getLocations(geoJSONPoint);
        if (req.length == 0)
            return null;
        return new GoogleLocationInformation(req[0]);
    }

    private String buildQuery(Address address) {
        StringBuilder stringBuilder = new StringBuilder();
        if (address.getStreet() != null) {
            stringBuilder.append(address.getStreet());
            stringBuilder.append(" ");
        }
        if (address.getHouseNumber() != null && address.getStreet() != null) {
            stringBuilder.append(address.getHouseNumber());
            stringBuilder.append(" ");
        }
        if (address.getCity() != null) {
            stringBuilder.append(address.getCity());
            stringBuilder.append(" ");
        }
        if (address.getPostalCode() != null) {
            stringBuilder.append(address.getPostalCode());
            stringBuilder.append(" ");
        }
        if (address.getState() != null) {
            stringBuilder.append(address.getState());
            stringBuilder.append(" ");
        }
        if (address.getCountry() != null) {
            stringBuilder.append(address.getCountry());
            stringBuilder.append(" ");
        }

        return stringBuilder.toString();
    }
}
