package com.kahlkepartner.genericgeocodinginterface.google;

import com.google.maps.model.AddressComponent;
import com.google.maps.model.AddressComponentType;
import com.google.maps.model.GeocodingResult;
import interfaces.AbstractLocationInformation;
import interfaces.data.Address;
import model.geometry.GeoJSONPoint;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Wrapper der Response einer Google Location Anfrage in das vereinbarte Interface
 * Bei der Implementation kann hier abhänig vom Wunsch entweder eine auf Gebrauchsparsing geschehen
 * oder ein Parsing durch den Konstruktor
 */
public class GoogleLocationInformation extends AbstractLocationInformation implements Serializable {

    GoogleLocationInformation(GeocodingResult result) {
        this.location = GeoJSONPoint.fromLngLat(result.geometry.location.lng, result.geometry.location.lat);
        this.address = fromAddressComponents(result.addressComponents);

    }

    @Override
    public Address getAddress() {
        return this.address;
    }


    @Override
    public HashMap<String, Object> getExtraInformation() {
        return null;
    }

    /**
     * Creates address components out of the address components.
     *
     * @param components
     * @return
     */
    private Address fromAddressComponents(AddressComponent[] components) {
        Address address = new Address();
        for (var component : components) {
            for (var type : component.types) {
                if (type.equals(AddressComponentType.ROUTE)) {
                    address.setStreet(component.longName);
                } else if (type.equals(AddressComponentType.STREET_NUMBER)) {
                    address.setHouseNumber(component.longName);
                } else if (type.equals(AddressComponentType.LOCALITY)) {
                    address.setCity(component.longName);
                } else if (type.equals(AddressComponentType.COUNTRY)) {
                    address.setCountry(component.longName);
                } else if (type.equals(AddressComponentType.POSTAL_CODE)) {
                    address.setPostalCode(component.longName);
                } else if (type.equals(AddressComponentType.ADMINISTRATIVE_AREA_LEVEL_1)) {
                    address.setState(component.longName);
                }
            }
        }
        return address;
    }

}
