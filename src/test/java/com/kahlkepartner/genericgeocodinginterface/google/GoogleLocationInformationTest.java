package com.kahlkepartner.genericgeocodinginterface.google;

import com.google.maps.errors.ApiException;
import com.kahlkepartner.googleapi.service.GoogleRouteService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

@SpringBootTest
class GoogleLocationInformationTest {
    @Autowired
    GoogleRouteService googleRouteService;

    @Test
    void getAddress() throws InterruptedException, ApiException, IOException, model.exeptions.ApiException {
        var res = googleRouteService.getLocations("Gabsheimer Weg 9 Biebelnheim");
        var firstRes = res[0];
        GoogleLocationInformation genericInformation = new GoogleLocationInformation(firstRes);
        genericInformation.getAddress();
    }
}
